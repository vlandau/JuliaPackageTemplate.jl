using Documenter, JuliaPackageTemplate

makedocs(
    modules = [JuliaPackageTemplate],
    checkdocs = :exports,
    sitename = "JuliaPackageTemplate.jl",
    pages = Any["index.md"],
    repo = "https://gitlab.com/vlandau/JuliaPackageTemplate.jl/blob/{commit}{path}#{line}"
)
