module JuliaPackageTemplate

export getA

"""
    getA()

A function that returns `:A`.
"""
getA() = :A

getB() = :B

getC() = :C

getD() = :D
end
