# Demonstrate the setup of Julia on Gitlab for CI

[![pipeline status](https://gitlab.com/vlandau/GitlabJuliaDemo.jl/badges/master/pipeline.svg)](https://gitlab.com/vlandau/JuliaPackageTemplate.jl/pipelines?scope=branches&page=1)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://vlandau.gitlab.io/JuliaPackageTemplate.jl/)
[![codecov](https://codecov.io/gl/vlandau/JuliaPackageTemplate.jl/branch/master/graph/badge.svg)](https://codecov.io/gl/vlandau/JuliaPackageTemplate.jl)

This is a minimal setup for a Julia package in Gitlab that has continuous integration and coverage stats via codecov set up.

[.gitlab-ci.yml](.gitlab-ci.yml) uses a Julia docker image to

1. build the package,
2. run tests and process coverage, submitting a covereage report to [codecov.io](codecov.io),
3. generate the documentation using [Documenter.jl](https://github.com/JuliaDocs/Documenter.jl), which is deployed at

    <https://vlandau.gitlab.io/JuliaPackageTemplate.jl/>
