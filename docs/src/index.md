# Overview

This package is a minimal working example of setting up CI and documentation generation for Julia projects using [Gitlab](https://gitlab.com/).

The package repository is <https://gitlab.com/vlandau/JuliaPackageTemplate.jl>.

This placeholder page demonstrates generated documentation.

```@docs
getA
```

# Version and timestamp

```@repl
VERSION
using Dates; now()
```

## Another heading 
With a bit of a description

## a smaller heading
with description

# another Big heading
some description

## subheading